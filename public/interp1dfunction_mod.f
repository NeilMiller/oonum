!! Author : Neil Miller
!!
!! Date   : September, 2012
!!
!! Purpose : The intent of this module is to create an interpolation 
!!           class.  I do not intend on actually implementing the interpolation
!!           but rather on using the interpolation routines that are built
!!           into MESA.  The objective is to create a simpler interface
!!           really only for myself.  
!!
!!           A part of my motivation is to create an interface 
!!           so that I don't have to figure out how Bill's code works
!!           every time I want to do an interpolation
!!           NOTE: This Code REQUIRES MESA or you need to write your own
!!                 interpolation routines and modify this accordingly
!!
!! WARNING : gfortran is only just starting to implement these oo features.
!!           It is likely they are not implemented efficiently and 
!!           possible that in some cases are incorrectly implemented

module interp1dfunction_mod
  use inverse_mod
  use const_def, only: dp
  implicit none
  
  type, extends(inverse) :: interp1dfunction
     integer, private :: npts
     real(dp), pointer, private :: xv(:) !! (npts)
     real(dp), pointer, private :: f1(:)  !! (4,npts)
     real(dp), pointer, private :: f(:,:)
     logical, private :: isSetup = .false.
     real(dp), private :: min_x, max_x, min_y, max_y
     real(dp) :: y_tol = 1d-10  !! write a function to modify this if you want
   contains
     !! Setup / Memory allocation & Destroy
     procedure :: Construct      => Construct1DInterp
     procedure :: Destroy        => Destroy1DInterp
     !! Maybe at some point you might want to write a routine that reads the
     !!  data from a file / and another that writes.  Ideally binary file format
     
     !! Define these to extend the inverse class
     procedure :: function_actual => interp_function_actual
     procedure :: get_ndim        => interp_get_ndim
     
     !! Define these to make the interpolation routine useful
     procedure :: get_domain      => interp_get_domain
     procedure :: get_range       => interp_get_range
     procedure :: interp          => interp_get_value
     procedure :: deriv           => interp_get_deriv
     procedure :: inv             => interp_inverse
     procedure :: get_npts        => interp_get_npts
  end type interp1dfunction
  
contains
  !! You may want to have a different constructor if you want to do a different
  !!  interpolation method
  subroutine Construct1DInterp(this, xdata, ydata, npts, ierr)
    use interp_1d_lib, only: interp_m3q
    use interp_1d_def, only: pm_work_size, mp_work_size
    implicit none
    class(interp1dfunction) :: this
    real(dp), intent(in), pointer, dimension(:) :: xdata, ydata
    integer, intent(in) :: npts
    integer, intent(out) :: ierr
      
    integer, parameter :: nwork = max(pm_work_size, mp_work_size)
    real(dp), pointer, dimension(:) :: work

    if(npts .le. 0) then
       write(*,*) "npts needs to be positive"
       ierr = -1
       return
    end if
    
    if(this%isSetup) then 
       write(*,*) " Replacing Interpolant"
       call this%Destroy()
    end if
    
    this%npts = npts
    
    allocate(this%xv(npts))
    allocate(this%f1(npts*4))
    allocate(work(npts*nwork))
    this%xv(1:npts) = xdata(1:npts)
    this%f(1:4,1:npts) => this%f1(1:4*npts)
    this%f(1,1:npts) = ydata(1:npts)
    call interp_m3q(this%xv, npts, this%f1, nwork, work, \
      'Construct1DInterp', ierr) 
    deallocate(work)
    
    this%min_x = min(xdata(1), xdata(npts))
    this%max_x = max(xdata(1), xdata(npts))
    this%min_y = min(ydata(1), ydata(npts))
    this%max_y = max(ydata(1), ydata(npts))
    this%isSetup = .true.
  end subroutine Construct1DInterp
  
  subroutine Destroy1DInterp(this)
    implicit none
    class(interp1dfunction) :: this

    if(associated(this%xv)) deallocate(this%xv)
    if(associated(this%f1)) deallocate(this%f1)
    nullify(this%f, this%f1, this%xv)
    this%isSetup = .false.  
  end subroutine Destroy1DInterp
  
  subroutine interp_function_actual(this, x_ptr, y_val, y_ind, ierr, dydx_ptr)
    use interp_1d_lib 
    implicit none
    class(interp1dfunction) :: this
    real(dp), pointer, dimension(:), intent(in) :: x_ptr
    real(dp), intent(out) :: y_val
    integer, intent(in) :: y_ind
    integer, intent(out) :: ierr
    real(dp), optional, pointer, dimension(:), intent(out) :: dydx_ptr
    
    real(dp) :: slope
    
    call interp_value_and_slope(this%xv, this%npts, this%f1, x_ptr(1), y_val, slope, ierr)
    if(present(dydx_ptr)) then 
       dydx_ptr(1) = slope       
    end if
  end subroutine interp_function_actual
  
  subroutine interp_get_ndim(this, ndim_out)
    implicit none
    class(interp1dfunction) :: this
    integer, intent(out) :: ndim_out
    
    ndim_out = 1
  end subroutine interp_get_ndim

  subroutine interp_get_domain(this, min_xval, max_xval, ierr)
    implicit none
    class(interp1dfunction) :: this
    real(dp), intent(out) :: min_xval, max_xval
    integer, intent(out) :: ierr

    if(this%isSetup) then
       min_xval = this%min_x
       max_xval = this%max_x
       ierr = 0
    else
       ierr = -1
    end if
  end subroutine interp_get_domain
  
  subroutine interp_get_range(this, min_yval, max_yval, ierr)
    implicit none
    class(interp1dfunction) :: this
    real(dp), intent(out) :: min_yval, max_yval
    integer, intent(out) :: ierr
    
    if(this%isSetup) then
       min_yval = this%min_y
       max_yval = this%max_y
       ierr = 0
    else
       ierr = -1
    end if
  end subroutine interp_get_range
  
  function interp_get_npts(this)
    implicit none
    integer :: interp_get_npts
    class(interp1dfunction) :: this  
    
    interp_get_npts = this%npts
  end function interp_get_npts
  
  subroutine interp_get_value(this, xval, yval, ierr)
    implicit none
    class(interp1dfunction) :: this
    real(dp), intent(in) :: xval
    real(dp), intent(out) :: yval
    integer, intent(out) :: ierr
    
    real(dp), target :: x_inp(1)
    real(dp), pointer, dimension(:) :: x_ptr
    integer :: y_ind = 1
    x_ptr => x_inp
    x_inp(1) = xval    
    
    call interp_function_actual(this, x_ptr, yval, y_ind, ierr)
    
  end subroutine interp_get_value
  
  subroutine interp_get_deriv(this, xval, deriv, ierr)
    implicit none
    class(interp1dfunction) :: this
    real(dp), intent(in) :: xval
    integer, intent(out) :: ierr
    real(dp), intent(out) :: deriv
    
    real(dp), target :: x_inp(1), dydx(1)
    real(dp), pointer, dimension(:) :: x_ptr, dydx_ptr
    real(dp) :: yval
    integer :: y_ind = 1
    x_ptr    => x_inp
    dydx_ptr => dydx
    
    x_inp(1) = xval
    call interp_function_actual(this, x_ptr, yval, y_ind, ierr, dydx_ptr=dydx_ptr)
    deriv = dydx(1)
  end subroutine interp_get_deriv


  subroutine interp_inverse(this, yval, xval, ierr, nstep)
    implicit none
    class(interp1dfunction) :: this
    real(dp), intent(in) :: yval
    real(dp), intent(out) :: xval
    integer, intent(out) :: ierr

    real(dp), target :: xfix(1)
    real(dp), pointer, dimension(:) :: xfix_ptr
    integer :: xvar_index = 1
    real(dp) :: xvar_guess
    integer, optional, intent(out) :: nstep
        
    integer:: y_ind = 1  
    
    xfix_ptr => xfix
    
    if((yval .le. this%max_y) .and.  &
         (yval .ge. this%min_y)) then
       xvar_guess = (this%max_x + this%min_x) / 2d0 !! start at the midpoint
       xval = this%inverse_function(xfix_ptr, xvar_index, yval, y_ind, &
            xvar_guess, this%min_x, this%max_x, this%y_tol, ierr, nstep=nstep)
    else
       ierr = -2  !! out of range
    endif
    
  end subroutine interp_inverse
end module interp1dfunction_mod
