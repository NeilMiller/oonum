! ***********************************************************************
!
!   Copyright (C) 2010  Bill Paxton
!
!   This file is part of MESA.
!
!   MESA is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Library Public License as published
!   by the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   MESA is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU Library General Public License for more details.
!
!   You should have received a copy of the GNU Library General Public License
!   along with this software; if not, write to the Free Software
!   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
!
! ***********************************************************************

!! Author   : Neil Miller
!!
!! Modified : September, 2012
!! 
!! Purpose  : This defines an object that store multiple 2D surfaces data, allows you to
!!            interpolate over them and allows you to perform inversions over the table
!!            This is a wrapps up the details of Bill's 2D Bicubic interpolation routines
!!
!! Thoughts for the future
!! In the future, it may make sense to have another layer of inheritance between the inverse
!! module and this module


module data_2dtable
  use inverse_mod
  use interp1dfunction_mod
  use const_def, only: dp
  implicit none

  integer, public, parameter :: ERR_FILE = 52
  integer, public, parameter :: ERR_BND = 53

  integer, private, parameter :: TBL_INTERP_SZ = 4
  integer, parameter :: TBL_NDERIV_OUT = 6

  !!! Indexes for Biquintic interpolation
  integer, parameter :: TBL_VAL = 1
  integer, parameter :: TBL_DX = 2
  integer, parameter :: TBL_DY = 3
  integer, parameter :: TBL_D2X = 4
  integer, parameter :: TBL_D2Y = 5
  integer, parameter :: TBL_DXDY = 6
  integer, private, parameter :: TBL_D2XDY = 7
  integer, private, parameter :: TBL_DXD2Y = 8
  integer, private, parameter :: TBL_D2XD2Y = 9
  
  real(dp), private, parameter :: ztol = 1d-5  

  type, public, extends(inverse) :: data_2dtbl
     private
     character (len=256) :: name, datafilename, Xname, Yname
     
     ! sizes of the arrays
     integer :: imax
     integer :: jmax
     integer :: NSurf !! Number of Z_i surfaces
     
     real(dp) :: minX, maxX, minY, maxY, dX, dY
     real(dp), dimension(:), pointer :: X_v, Y_v
     
     type(interp1dfunction) :: minX_fun, maxX_fun, minY_fun, maxY_fun
     logical :: use_minX_fun = .false. , &
                use_maxX_fun = .false. , &
                use_minY_fun = .false. , &
                use_maxY_fun = .false.
     
     !!   (4, imax, jmax, NSurf) or (9, imax, jmax, NSurf) 
     real(dp), dimension(:), pointer :: f1_ary
     real(dp), dimension(:,:,:,:), pointer :: Zi_m
     integer, dimension(:,:), pointer :: spec_fields 
     
     !!   (6, NSurf)
     !!   This array is allocated for the specific table as a place to store the values when
     !!   requested
     real(dp), dimension(:,:), pointer :: Zi_dv
     
     integer :: Xlog, Ylog
!     real(dp), dimension(:,:), pointer :: BadVal
     
     integer :: ilinx, iliny

     !! Has the table's members been allocated?
     logical :: IsSetup = .false.
     !! Has the table been loaded?
     logical :: IsLoaded = .false.
     !! If any smoothing is performed, the interpolation setup routine needs to be run again
     logical :: InterpolIsSetup = .false.     

   contains
     !! Allocate memory, load binary data, setup interpolation 
     procedure, public :: Construct       => Construct2DInterp
     !! Deallocate data
     procedure, public :: Destroy         => Destroy2DInterp

     procedure, public :: set_minX_fun 
     procedure, public :: set_maxX_fun
     procedure, public :: set_minY_fun
     procedure, public :: set_maxY_fun
     
     procedure :: function_actual => interp2D_function_actual
     procedure :: get_ndim        => interp2d_get_ndim
     
     procedure, public :: get_val         => get_2Dtable_val
     procedure, public :: inv_X           => invX_2Dtbl  
     procedure, public :: inv_Y           => invY_2Dtbl


  end type data_2dtbl
  
contains
  subroutine Construct2DInterp(tbl, Xsize, Ysize, NSurf, &
       filename, name, Xname, Yname, ierr, xlog, ylog)
    class (data_2dtbl), intent(in) :: tbl  !! This should already be allocated
    character (len=256), intent(in) :: filename, name, Xname, Yname
    integer, intent(in) :: Xsize, Ysize
    integer, intent(in) :: NSurf
    integer, intent(in), optional :: xlog, ylog
    integer, intent(out) :: ierr ! 0 means OK       
    
    integer :: xlog1, ylog1
    
    call alloc_2Dtable_mem(tbl, Xsize, Ysize, NSurf, &
         filename, name, Xname, Yname, ierr)

    if(present(xlog)) then
       xlog1 = xlog
    else
       xlog1 = 0
    endif

    if(present(ylog)) then 
       ylog1 = ylog
    else
       ylog1 = 0
    endif
    
    call load_2Dtable_data(tbl, NSurf, xlog1, ylog1, ierr)
    
    call make_2Dtable_bicub(tbl, ierr)
  end subroutine Construct2DInterp
  
  
  subroutine alloc_2Dtable_mem(tbl, Xsize, Ysize, NSurf, &
       filename, name, Xname, Yname, ierr)
      !! This routine just allocates the memory
    use utils_lib, only: mesa_error
    
    class (data_2dtbl) :: tbl  !! This should already be allocated
    character (len=256), intent(in) :: filename, name, Xname, Yname
    integer, intent(in) :: Xsize, Ysize
    integer, intent(in) :: NSurf
    integer, intent(out) :: ierr ! 0 means OK      
      
    ierr = 0
      
    tbl%imax = Xsize
    tbl%jmax = Ysize
    tbl%NSurf = NSurf
    tbl%datafilename = filename
    tbl%name = name
    tbl%Xname = Xname
    tbl%Yname = Yname
      
    write(*,*) tbl%imax, tbl%jmax
      
    write(*,*) "Allocating table internal memory"
    call alloc_1d_array(tbl%X_v, Xsize)
    call alloc_1d_array(tbl%Y_v, Ysize)
    allocate(tbl%f1_ary(TBL_INTERP_SZ * Xsize * Ysize * tbl%NSurf))
    tbl%Zi_m(1:TBL_INTERP_SZ,1:Xsize,1:Ysize,1:(tbl%Nsurf)) => &
         tbl%f1_ary(1:(TBL_INTERP_SZ*Xsize*Ysize*tbl%Nsurf))
    allocate(tbl%Zi_dv(TBL_NDERIV_OUT, tbl%NSurf)) !! NOT 6
    allocate(tbl%spec_fields(TBL_INTERP_SZ, tbl%NSurf))
    if(ierr /= 0) then
       call mesa_error(__FILE__,__LINE__,'failure to allocate memory for storing table')
    endif
    tbl%Zi_m(:,:,:,:) = 0d0
    tbl%IsSetup = .true.
      
  contains      
    subroutine alloc_1d_array(ptr,sz)
      real(dp), dimension(:), pointer :: ptr
      integer, intent(in) :: sz        
      allocate(ptr(sz),stat=ierr)
      if (ierr /= 0) then
         call mesa_error(__FILE__,__LINE__,&
              'failure in attempt to allocate Aneos_Table storage')
      endif
    end subroutine alloc_1d_array
  end subroutine alloc_2dtable_mem
    
  subroutine Destroy2DInterp(tbl)
    class (data_2dtbl) :: tbl
    
    if(associated(tbl%X_v)) deallocate(tbl%X_v)
    if(associated(tbl%Y_v)) deallocate(tbl%Y_v)
    if(associated(tbl%f1_ary)) deallocate(tbl%f1_ary)
    nullify(tbl%Zi_m)
    if(associated(tbl%Zi_dv)) deallocate(tbl%Zi_dv)
    if(associated(tbl%spec_fields)) deallocate(tbl%spec_fields)
  end subroutine Destroy2DInterp
    
    !! The filename should have a very specific format!!
    !! It should be evenly spaced in X and Y space (whatever X and Y are)
    !! The columns should read
    !!   Xvalue   Yvalue   Z(SurfaceIndex(1))_DerivIndex(1) Z2_value ...
    !!   Xvalue(+1) Yvalue Z1_value   Z2_value
    !!   ....
    !!   Xvalue   Yvalue(+1) Z1_value Z2_value
    
  subroutine load_2Dtable_data(tbl, NSurf, Xlog, Ylog, ierr)
      !! This subroutine loads the data from the file      
    use utils_lib, only: mesa_error
    use interp_2d_lib_db
    
    class (data_2dtbl) :: tbl
    integer, intent(in) :: NSurf
    integer, intent(in) :: Xlog, Ylog
    integer, intent(out) :: ierr
    
    character (len=256) :: message, filename
    integer :: i,j,k,ios,fid !!! Indices and file IDs
    
    !! Need these dummy variables to load doubles from file
    real(dp) :: xval, yval
    real(dp), pointer, dimension(:) :: Zvect
    
    if(tbl%IsSetup .eqv. .false.) then
       write(*,*) "This table structure has been allocated, but its members have not"
       stop
    endif
    
    call alloc_1d_array(Zvect,tbl%NSurf)
    
    tbl%Xlog = Xlog  !! 1 if the grid is logrithmically spaced
    tbl%Ylog = Ylog  !! 0 if the grid is linearly spaced
    if(tbl%Xlog .eq. 1) then 
       tbl%ilinx = 0
    else
       tbl%ilinx = 1
    endif
    if(tbl%Ylog .eq. 1) then
       tbl%iliny = 0
    else
       tbl%iliny = 1
    endif
    
    filename = tbl%datafilename
      
    ierr = 0
      
    ios = 0
    fid = 20
    write(*,*) "Opening file -",  trim(filename), "-"
    open(unit=fid, file=trim(filename), action='read',status='old',iostat=ios, &
         form="unformatted")
    
    if(ios .ne. 0) then 
       write(message,'(3a,i6)') 'failed to open ', trim(filename), ' : ios ', ios
       ierr = ERR_FILE
       write(*,*) "Failed to open file"
       call mesa_error(__FILE__,__LINE__,message)
       return
    end if
      
!!! Read the file and put the values into a temporary matrix
    do i=1,(tbl%imax)
       do j=1,(tbl%jmax)
          read(fid) xval, yval, Zvect
          tbl%X_v(i) = xval
          tbl%Y_v(j) = yval
          do k=1,NSurf
             if(.not. isnan(Zvect(k))) then                  
                tbl%Zi_m(TBL_VAL,i,j,k) = Zvect(k)
             else 
                write(*,*) "NaN value found"
                stop
             endif
          end do
       enddo
    enddo
    tbl%minX = tbl%X_v(1)
    tbl%maxX = tbl%X_v(tbl%imax)
    tbl%minY = tbl%Y_v(1)
    tbl%maxY = tbl%Y_v(tbl%jmax)
    tbl%dx = tbl%X_v(2)-tbl%X_v(1)
    tbl%dy = tbl%Y_v(2)-tbl%Y_v(1)
      
    do i=2,(tbl%imax)
       if(tbl%X_v(i).le.tbl%X_v(i-1)) then
          write(*,*) "The X dimension is not monotonic"
          write(*,*) tbl%imax, tbl%jmax
          stop
       endif
    enddo
    
    do j=2,(tbl%jmax)
       if(tbl%Y_v(j).le.tbl%Y_v(j-1)) then
          write(*,*) "The Y dimension is not monotonic"
          stop
       endif
    enddo
    write(*,*) "Finished reading file, ierr = ", ierr
    call do_free(Zvect)
    close(unit=fid)  !! Done with file
    
    if(ierr.eq.0) then
       tbl%IsLoaded = .true.
       write(*,*) "Success loading table  ", tbl%name
    else 
       write(*,*) "Failed to load table ", tbl%name
       tbl%IsLoaded = .false.  !! Redundant
       stop
    end if
    
  contains
    subroutine alloc_1d_array(ptr,sz)
      real(dp), dimension(:), pointer :: ptr
      integer, intent(in) :: sz
      allocate(ptr(sz),stat=ierr)
      call mesa_error(__FILE__,__LINE__,'failure in attempt to allocate Aneos_Table storage')
    end subroutine alloc_1d_array

    subroutine do_free(array_ptr)
      real(dp), pointer :: array_ptr(:)
      if (associated(array_ptr)) deallocate(array_ptr)
    end subroutine do_free
  end subroutine load_2Dtable_data

    !!! This procedure sets up the bicubic spline
    !!! This must be a different procedure than "aneos_load_table" since
    !!! we may want to perform smoothing in specific regions first
  subroutine make_2Dtable_bicub(tbl, ierr)
    use utils_lib, only: mesa_error
    use interp_2d_lib_db
    
      class (data_2dtbl) :: tbl
      integer, intent(out) :: ierr
      integer :: imax, jmax
      real(dp), dimension(:), pointer :: bcxvals
      real(dp), dimension(:), pointer :: bcyvals
      integer :: bcflag 
      integer :: k
      
      real(dp), dimension(:), pointer :: f1
      integer :: blc_sz

      if(.not. tbl%IsLoaded) then 
         write(*,*) "You are attempting to make the bicubic spline before loading the table"
         write(*,*) "table name: ", tbl%name
         stop
      endif

      ierr = 0
      imax = tbl%imax
      jmax = tbl%jmax
      
      call alloc_1d_array(bcxvals,imax) !! boundary condition arrays requested by mkinterp
      call alloc_1d_array(bcyvals,jmax)

      bcflag = 4
      bcxvals(:) = 0.
      bcyvals(:) = 0.
      tbl%ilinx = 1
      tbl%iliny = 1
      

      blc_sz = (TBL_INTERP_SZ * tbl%imax * tbl%jmax)
      do k=1,tbl%NSurf
         f1(1 : blc_sz) => tbl%f1_ary(((k-1) * blc_sz + 1) : (k * blc_sz))
         call interp_mkbicub_db(tbl%X_v, tbl%imax, tbl%Y_v, tbl%jmax, &
              f1, tbl%imax, &
              bcflag, bcxvals, bcflag, bcxvals, bcflag, bcyvals, bcflag, bcyvals, &
              tbl%ilinx, tbl%iliny, ierr)
      enddo
      tbl%InterpolIsSetup = .true.
      
      call do_free(bcxvals)
      call do_free(bcyvals)

    contains
      subroutine alloc_1d_array(ptr,sz)
        real(dp), dimension(:), pointer :: ptr
        integer, intent(in) :: sz
        allocate(ptr(sz),stat=ierr)
        call mesa_error(__FILE__,__LINE__,'failure in attempt to allocate Aneos_Table storage')
      end subroutine alloc_1d_array

      subroutine do_free(array_ptr)
        real(dp), pointer :: array_ptr(:)
        if (associated(array_ptr)) deallocate(array_ptr)
      end subroutine do_free
    end subroutine make_2Dtable_bicub
    
    subroutine set_minX_fun(this, YVect, XVect, npts, ierr)
      class (data_2Dtbl) :: this
      real(dp), intent(in), dimension(:), pointer :: YVect, XVect
      integer, intent(in) :: npts
      integer, intent(out) :: ierr
      real(dp) :: min_yval, max_yval
      
      call this%minX_fun%Construct(YVect, XVect, npts, ierr)
      if(ierr .ne. 0) then
         call this%minX_fun%get_domain(min_yval, max_yval, ierr)
         if((ierr .eq. 0) .and. (min_yval .le. this%minY) .and. (max_yval .ge. this%maxY)) then
            this%use_minX_fun = .true.
         else
            ierr = -1
         endif
      endif
    end subroutine set_minX_fun

    subroutine set_maxX_fun(this, YVect, XVect, npts, ierr)
      class (data_2Dtbl) :: this
      real(dp), intent(in), dimension(:), pointer :: YVect, XVect
      integer, intent(in) :: npts
      integer, intent(out) :: ierr
      real(dp) :: min_yval, max_yval

      call this%maxX_fun%Construct(YVect, XVect, npts, ierr)
      if(ierr .ne. 0) then
         call this%maxX_fun%get_domain(min_yval, max_yval, ierr)
         if((ierr .eq. 0) .and. (min_yval .le. this%minY) .and. (max_yval .ge. this%maxY)) then
            this%use_maxX_fun = .true.
         else
            ierr = -1
         endif
      endif
    end subroutine set_maxX_fun

    subroutine set_minY_fun(this, XVect, YVect, npts, ierr)
      class (data_2Dtbl) :: this
      real(dp), intent(in), dimension(:), pointer :: XVect, YVect
      integer, intent(in) :: npts
      integer, intent(out) :: ierr
      real(dp) :: min_xval, max_xval

      call this%minY_fun%Construct(XVect, YVect, npts, ierr)
      if(ierr .ne. 0) then 
         call this%minY_fun%get_domain(min_xval, max_xval, ierr)
         if((ierr .eq. 0) .and. (min_xval .le. this%minX) .and. (max_xval .ge. this%maxX)) then
            this%use_minY_fun = .true.
         else
            ierr = -1
         end if
      end if
    end subroutine set_minY_fun

    subroutine set_maxY_fun(this, XVect, YVect, npts, ierr)
      class (data_2Dtbl) :: this
      real(dp), intent(in), dimension(:), pointer :: XVect, YVect
      integer, intent(in) :: npts
      integer, intent(out) :: ierr
      real(dp) :: min_xval, max_xval

      call this%maxY_fun%Construct(XVect, YVect, npts, ierr)      
      if(ierr .ne. 0) then
         call this%maxY_fun%get_domain(min_xval, max_xval, ierr)
         if((ierr .eq. 0) .and. (min_xval .le. this%minX) .and. (max_xval .ge. this%maxX)) then
            this%use_maxY_fun = .true.
         else
            ierr = -1
         end if
      end if
    end subroutine set_maxY_fun
    
    subroutine interp2D_function_actual(this, x_ptr, y_val, y_ind, ierr, dydx_ptr)
      class (data_2Dtbl) ::this
      real(dp), pointer, dimension(:), intent(in) :: x_ptr
      real(dp), intent(out) :: y_val
      integer, intent(in) :: y_ind
      integer, intent(out) :: ierr
      real(dp), optional, pointer, dimension(:), intent(out) :: dydx_ptr

      real(dp) :: XVal, YVal
      real(dp), pointer, dimension(:,:) :: Zi_dv

      real(dp) :: minX, maxX, minY, maxY

      XVal = x_ptr(1)
      YVal = x_ptr(2)
      
      !! Check that Xval and YVal are within the table range
      if((Xval .le. this%maxX) .and. (Xval .ge. minX) &
           .and. (Yval .le. this%maxY) .and. (Yval .ge. this%minY)) then
         if(this%use_minX_fun) then
            call this%minX_fun%interp(YVal, minX, ierr)
            if(Xval .lt. minX) then
               ierr = ERR_BND
               return
            endif
         endif

         if(this%use_maxX_fun) then
            call this%maxX_fun%interp(Yval, maxX, ierr)
            if(Xval .gt. maxX) then
               ierr = ERR_BND
               return
            endif
         endif

         if(this%use_minY_fun) then
            call this%minY_fun%interp(Xval, minY, ierr)
            if(Yval .lt. minY) then
               ierr = ERR_BND
               return
            endif
         endif
         
         if(this%use_maxY_fun) then
            call this%maxY_fun%interp(Xval, maxY, ierr)
            if(Yval .gt. maxY) then
               ierr = ERR_BND
               return
            end if
         endif
      else
         ierr = ERR_BND
         return
      endif
      allocate(Zi_dv(TBL_NDERIV_OUT, this%NSurf))
      
      call get_2Dtable_val(this, XVal, YVal, Zi_dv, ierr, y_ind=y_ind)
      
      y_val = Zi_dv(TBL_VAL,y_ind)
      
      if(present(dydx_ptr)) then
         dydx_ptr(1) = Zi_dv(TBL_DX,y_ind)
         dydx_ptr(2) = Zi_dv(TBL_DY,y_ind)
      endif
      
      deallocate(Zi_dv)
      
    end subroutine interp2D_function_actual

    subroutine interp2d_get_ndim(this, ndim_out)
      class(data_2dtbl) :: this
      integer, intent(out) :: ndim_out
      
      ndim_out = 2
    end subroutine interp2d_get_ndim
    
    
    !! This routine uses the bicubic spline interpolation routine to fill out the Zi_dv matrix
    !! this matrix is a (NSurf, 6) dimension matrix where the values are
    !!   Zi, dZi/dX, dZi/dY, d2Zi/dX2, d2Zi/dY2, d2Zi/(dX dY)
    !! The design idea is then that the equation of state will interface with the table to
    !! lookup values, but then it will perform the necessary operations to produce the results
    !! vector
    subroutine get_2Dtable_val(tbl, Xval, Yval, Zi_dv, ierr, y_ind)
      use utils_lib, only: mesa_error
      use interp_2d_lib_db
      
      class (data_2dtbl), intent(in) :: tbl
      real(dp), intent(in) :: Xval, Yval
      real(dp), intent(out), pointer, dimension(:,:) :: Zi_dv      
      integer, intent(out) :: ierr
      integer, optional, intent(in) :: y_ind

      real(dp), pointer, dimension(:) :: f1
      integer :: i
      integer :: ict(6)      
      integer :: blc_sz 

      if(.not.associated(Zi_dv)) then
         write(*,*) "You MUST allocate the Zi_dv matrix and deallocate as necessary at the higher level."
         stop
      endif

      if(.not.tbl%IsSetup) then
         write(*,*), "You need to call the procedure alloc_2Dtable_mem - this will allocate the member function memory"
         stop
      endif

      if(.not.tbl%IsLoaded) then
         write(*,*), " You didn't actually load the table"
         stop
      endif

      if(.not.tbl%InterpolIsSetup) then
         write(*,*), "INTERPOLATION NOT SETUP - THERE IS A BUG IN YOUR CODE"
         stop
      endif

      if((Xval.gt.tbl%maxX).or.(Xval.lt.tbl%minX) &
           .or.(Yval.gt.tbl%maxY).or.(Yval.lt.tbl%minY)) then
         !write(*,*) "data 2Dtable get out of bounds.  Please check that you have initiated this correctly are using it with values inside the range"
         write(*,*) "Data table out of bounds ", tbl%name
         write(*,*) "Xrange: ", tbl%minX, tbl%maxX
         write(*,*) "Yrange: ", tbl%minY, tbl%maxY
         write(*,*) "Xvalue: ", Xval
         write(*,*) "Yvalue: ", YVal
         stop
      endif

      !! Call the bicubic spline interpolation
      ict(:) = 1

      blc_sz = (TBL_INTERP_SZ*tbl%imax*tbl%jmax)      
      if(present(y_ind)) then !! Only perform the lookup for this one index
         f1(1 : blc_sz) => tbl%f1_ary(((y_ind-1)*blc_sz + 1):(y_ind*blc_sz))
         call interp_evbicub_db(Xval, Yval, tbl%X_v, tbl%imax, tbl%Y_v, tbl%jmax, &
              tbl%ilinx,tbl%iliny,f1, tbl%imax, ict, Zi_dv(:,y_ind), ierr)         
         if(ierr/=0) call do_stop('interpolation failure')
      else
         do i=1,tbl%NSurf
            f1(1 : blc_sz) => tbl%f1_ary(((i-1)*blc_sz + 1):(i*blc_sz))

            call interp_evbicub_db(Xval, Yval, tbl%X_v, tbl%imax, tbl%Y_v, tbl%jmax, &
                 tbl%ilinx,tbl%iliny, f1, tbl%imax, ict, Zi_dv(:,i), ierr)
            if(ierr/=0) call do_stop('interpolation failure')
         end do
      end if
    contains
      subroutine do_stop(str)
        character (len=*) :: str
        write(*,*) trim(str)
        stop 1
      end subroutine do_stop
    end subroutine get_2Dtable_val
    
    !! Incomplete function
    subroutine invX_2Dtbl(tbl, Xvalue, Zvalue, Zindex, Yvalue, ierr, set_ztol)
      implicit none
      class (data_2dtbl), intent(in) :: tbl
      real(dp), intent(in) :: Xvalue, Zvalue
      integer, intent(in) :: Zindex
      real(dp), intent(out) :: Yvalue
      integer, intent(out) :: ierr
      real(dp), intent(in), optional :: set_ztol

      real(dp), target :: xfixed(2)
      real(dp), pointer, dimension(:) :: xfixed_ptr
      integer :: xvar_index
      real(dp) :: y_tol, xvar_guess, y_tgt, minY_bnd, maxY_bnd
      xfixed_ptr => xfixed

      xfixed_ptr(1) = Xvalue
      xvar_index = 2
      y_tgt = Zvalue
      if(present(set_ztol)) then
         y_tol = set_ztol
      else
         y_tol = ztol 
      end if

      
      if(tbl%use_minY_fun) then
         call tbl%minY_fun%interp(Xvalue, minY_bnd, ierr)
         if(minY_bnd .le. tbl%minY) minY_bnd = tbl%minY
      else 
         minY_bnd = tbl%minY
      endif
      
      if(tbl%use_maxY_fun) then
         call tbl%maxY_fun%interp(Xvalue, maxY_bnd, ierr)
         if(maxY_bnd .gt. tbl%maxY) maxY_bnd = tbl%maxY
      else
         maxY_bnd = tbl%maxY
      endif

      xvar_guess = (maxY_bnd + minY_bnd) / 2d0


      Yvalue = inverse_function(tbl, xfixed_ptr, xvar_index, y_tgt, Zindex,  &
           xvar_guess, minY_bnd, maxY_bnd, y_tol, ierr)
    end subroutine invX_2Dtbl
    
    subroutine invY_2Dtbl(tbl, Yvalue, Zvalue, Zindex, Xvalue, ierr, set_ztol)
      implicit none
      class (data_2dtbl), intent(in) :: tbl
      real(dp), intent(in) :: Yvalue, Zvalue
      integer, intent(in) :: Zindex
      real(dp), intent(out) :: Xvalue
      integer, intent(out) :: ierr
      real(dp), intent(in), optional :: set_ztol

      real(dp), target :: xfixed(2)
      real(dp), pointer, dimension(:) :: xfixed_ptr
      integer:: xvar_index
      real(dp) :: y_tol, xvar_guess, y_tgt, minX_bnd, maxX_bnd
      xfixed_ptr => xfixed

      xfixed_ptr(2) = Yvalue
      xvar_index = 1      
      y_tgt = Zvalue
      if(present(set_ztol)) then
         y_tol = set_ztol
      else
         y_tol = ztol
      end if

      if(tbl%use_minX_fun) then
         call tbl%minX_fun%interp(Yvalue, minX_bnd, ierr)
         if(minX_bnd .le. tbl%minX) minX_bnd = tbl%minX
      else
         minX_bnd = tbl%minX
      endif
      
      if(tbl%use_maxX_fun) then
         call tbl%maxX_fun%interp(Yvalue, maxX_bnd, ierr)
         if(maxX_bnd .gt. tbl%maxX) maxX_bnd = tbl%maxX
      else
         maxX_bnd = tbl%maxX
      endif
      
      xvar_guess = (minX_bnd + maxX_bnd) / 2d0
      
      Xvalue = inverse_function(tbl, xfixed_ptr, xvar_index, y_tgt, Zindex, &
           xvar_guess, minX_bnd, maxX_bnd, y_tol, ierr)
      
    end subroutine invY_2Dtbl
  end module data_2Dtable
