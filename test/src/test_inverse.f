!! Author : Neil Miller
!!
!! Date   : September, 2012
!!
!! Test that the inverse routine works 
!!

module test_inverse_mod
  use inverse_mod
  use const_def, only: dp
  implicit none
  
  type, extends(inverse) :: test_poly
     real(dp), private :: alpha, beta, order
   contains
     procedure :: function_actual => my_poly
     procedure :: get_ndim => p_ndim
  end type test_poly

contains

  subroutine my_poly(this, x_ptr, y_val, y_ind, ierr, dydx_ptr)
    class(test_poly) :: this
    real(dp), pointer, dimension(:), intent(in) :: x_ptr
    real(dp), intent(out) :: y_val
    integer, intent(in) :: y_ind
    integer, intent(out) :: ierr
    real(dp), optional, pointer, dimension(:), intent(out) :: dydx_ptr

    
    y_val = this%alpha  + this%beta * x_ptr(1)**(this%order)
    if(present(dydx_ptr)) then
       dydx_ptr(1) = (this%order) * this%beta * x_ptr(1)**(this%order-1)
    endif
  end subroutine my_poly
  
  subroutine p_ndim(this, ndim_out)
    class(test_poly) :: this
    integer, intent(out) :: ndim_out
    
    ndim_out = 1
  end subroutine p_ndim

  function ConstructPoly(alpha, beta, order)
    type(test_poly) :: ConstructPoly
    real(dp) :: alpha, beta, order
    
    ConstructPoly%alpha = alpha
    ConstructPoly%beta = beta
    ConstructPoly%order = order
  end function ConstructPoly
  
end module test_inverse_mod


program test_inverse
  use test_inverse_mod
  implicit none

  type(test_poly) :: test_cub, test_quadratic
  real(dp), target :: xvar(1)
  real(dp), pointer, dimension(:) :: x_ptr 
  real(dp) :: ytgt, xvar_guess, xvar_min, xvar_max, y_tol, xans
  integer :: ierr, nstep, xvar_index
  
  integer :: y_ind

  test_cub = ConstructPoly(1d0, 5d0, 3d0)
  test_quadratic = ConstructPoly(0d0, 2d0, 2d0)

  x_ptr => xvar
  xvar_index = 1
  ytgt = 20d0
  xvar_guess = 0d0
  xvar_min = -100d0
  xvar_max = 100d0
  y_tol = 1d-10

  xans =  test_cub%inverse_function(x_ptr, xvar_index, ytgt, y_ind, &
       xvar_guess, xvar_min, xvar_max, y_tol, ierr, nstep=nstep)
  
  write(*,*) "ierr    = ", ierr
  write(*,*) "Xanswer = ", xans
  write(*,*) "nstep   = ", nstep

  ytgt = -1d0
  xans = test_quadratic%inverse_function(x_ptr, xvar_index, ytgt, y_ind, &
       xvar_guess, xvar_min, xvar_max, y_tol, ierr, nstep=nstep)
  
  write(*,*) "ierr    = ", ierr
  write(*,*) "Xanswer = ", xans
  write(*,*) "nstep   = ", nstep
  
end program test_inverse
