!! Author : Neil Miller
!!
!! Purpose: Test the 2D table object
!!          1. Write data to a file
!!          2. Read data with data_2Dtable
!!          3. Run the interpolation and compare to analytic value
!!          4. Test inverse routines
!! 
!! Date  : September 30 - This does now appear to be working

module test_mod
  use const_def
  
contains
  function fun_a(xval, yval)
    
    real(dp) :: fun_a
    real(dp), intent(in) :: xval, yval
    
    fun_a = exp(xval) * yval
  end function fun_a
  
  function fun_b(xval, yval)
    real(dp) :: fun_b
    real(dp), intent(in) :: xval, yval
    
    fun_b = (xval - 5d0) * yval**3d0
  end function fun_b

end module test_mod

program test_2Dtbl
  use data_2Dtable
  use test_mod
  
  integer :: Xsize, Ysize, i, j, fid, ios, zind
  real(dp) :: xmin, xmax, ymin, ymax, dx, dy, xval, yval, zval_a, zval_b, &
       invzval, yval2, xval2
  character(len=256) :: filename, name, Xname, Yname
  
  type(data_2dtbl) :: mytbl
  integer :: ierr

  integer, parameter :: NSurf = 2
  real(dp), target :: Z_dv(TBL_NDERIV_OUT,NSURF)
  real(dp), pointer :: Z_dv_ptr(:,:)
  Z_dv_ptr => Z_dv

  filename = "test2Dsurf.binary_data"
  name = "TestSurface"
  Xname = "Xvalue"
  Yname = "Yvalue"
  
  Xsize = 501
  Ysize = 501
  xmin = -1d1
  xmax = 1d1
  ymin = -1d1
  ymax = 1d1

  dx = (xmax - xmin) / (Xsize - 1)
  dy = (ymax - ymin) / (Ysize - 1)
  

  fid = 10
  open(fid, file=filename, action="write", form="unformatted", iostat=ios)
  
  do i=1,Xsize
     xval = xmin + dx * (i-1)
     do j=1, Ysize
        yval = ymin + dy * (j-1)
        write(fid) xval, yval, fun_a(xval, yval), fun_b(xval, yval)
     enddo
  enddo
  close(fid)
  
  call mytbl%Construct(Xsize, Ysize, NSurf, filename, &
       name, Xname, Yname, ierr)
  
  !! Part 3 and 4
  !! Repurpose Xsize and Ysize, xmin, xmax, ymin, ymax, dx, dy 
  !!  for pinging the table
  
  Xsize = 37
  Ysize = 37
  xmin = 1d0
  xmax = 5d0
  ymin = 1d0
  ymax = 5d0
  
  dx = (xmax - xmin) / (Xsize - 1)
  dy = (ymax - ymin) / (Ysize - 1)

  zind = 1
  
  do i=1,Xsize
     xval = xmin + dx * (i-1)
     do j=1,Ysize
        yval = ymin + dy * (j-1)
        
        call mytbl%get_val(xval, yval, Z_dv_ptr, ierr)
        zval_a = fun_a(xval, yval)
        zval_b = fun_b(xval, yval)

        call mytbl%inv_X(xval, zval_a, zind, yval2, ierr)
        
        call mytbl%inv_Y(yval, zval_a, zind, xval2, ierr)
        write(*,*) xval - xval2, yval - yval2, &
             (Z_dv_ptr(TBL_VAL,1) - zval_a), &
             (Z_dv_ptr(TBL_VAL,2) - zval_b), ierr
        
     end do
  end do
  
  call mytbl%Destroy
  
end program test_2Dtbl
