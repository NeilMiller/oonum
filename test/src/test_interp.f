program test
  use interp1dfunction_mod
  use const_def, only: dp

  implicit none

  type(interp1dfunction) :: MyFunc

  integer, parameter :: Nvals = 101  
  real(dp), target :: xvals(Nvals), yvals(Nvals)
  real(dp), pointer, dimension(:) :: x_ptr, y_ptr
  real(dp) :: xpt, ypt
  integer :: i, ierr, nstep

  do i=1,Nvals
     xvals(i) = 0.1d0* (i-1)
     yvals(i) = xvals(i)**2d0 
     write(*,*) xvals(i), yvals(i)
  enddo

  x_ptr => xvals
  y_ptr => yvals

  call MyFunc%Construct(x_ptr, y_ptr, Nvals, ierr)
  do i=1,100
     xpt = 1d0 + 0.01 * real(i)
     call MyFunc%interp(xpt, ypt, ierr)
     write(*,*) "interp: ", xpt, ypt, ierr, 2d0 * xpt

     call MyFunc%inv(ypt, xpt, ierr, nstep=nstep)
     write(*,*) "   inv: ", ypt, xpt, ierr, nstep
  enddo
  call MyFunc%Destroy

end program test
